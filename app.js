const express = require("express");
const { sequelize } = require("./models");

const app = express();

// Middleware
app.use(express.json()); // parse json bodies in the request object

// Redirect requests to endpoint starting with /posts to postRoutes.js
app.use("/", require("./routes/APIRoutes"));

// Global Error Handler. IMPORTANT function params MUST start with err
app.use((err, req, res, next) => {
  console.log(err.stack);
  console.log(err.name);
  console.log(err.code);

  res.status(500).json({
    message: "Something went really wrong",
  });
});

// Listen on pc port
const PORT = process.env.PORT || 9000;
app.listen(PORT, async () => {
  console.log(`Server running on localhost PORT ${PORT}`);
  await sequelize.sync({ force: true }); //Updates the database structure if changes are made.
  // console.log("MySQL database synced");
});
