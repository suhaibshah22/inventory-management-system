/*
Title: API Controllers
Created At: 11/10/2021

This file contains all of the API routes for making DB Queries such as creating entries for new items as well as 
updating locations, and deleting items. 

*/
const {
  sequalize,
  Item,
  Location,
  unreg_item,
  unreg_location,
} = require("../models");

exports.getAllItems = async (req, res, next) => {
  console.log("Request received for fetching all items in DB");
  try {
    res.status(200).json({ allItems });
  } catch (error) {
    console.log(error);
    res.status(500).send("Invalid Request");
    next(error);
  }
};

//-----------------------------General Purpose functions.------------------------------------

// const CreateUnregLocation = async (name, rfid)=>{

//   let { name, rfid } = req.body;

//   let no_of_items = 0; //Initial Value.

//   try {
//     //Delete the location (being registered) from the unregistered locations table.
//     let toDelete = await unreg_location.findOne({ where: { rfid } });
//     console.log(toDelete);
//     //In testing, sometimes a random rfid is sent which can't be found in the unreglocations table.
//     if (toDelete != null) {
//       await unreg_location.destroy({ where: { rfid } });
//     }

//     //Register the location and put it in the locations table.
//     const location = await Location.create({
//       name,
//       no_of_items,
//       rfid,
//     });

// };

// const RegisterLocation = async (name, rfid, no_of_items )=>{

//   let { name, rfid } = req.body;

//   let no_of_items = 0; //Initial Value.

//   try {
//     //Delete the location (being registered) from the unregistered locations table.
//     let toDelete = await unreg_location.findOne({ where: { rfid } });
//     console.log(toDelete);
//     //In testing, sometimes a random rfid is sent which can't be found in the unreglocations table.
//     if (toDelete != null) {
//       await unreg_location.destroy({ where: { rfid } });
//     }

//     //Register the location and put it in the locations table.
//     const location = await Location.create({
//       name,
//       no_of_items,
//       rfid,
//     });

// };

exports.itemInbound = async (req, res, next) => {
  /*When an item is scanned as inbound at a particular location, 
    the database is first queried to see if the item has been previously registered (has been given a name).
    --If the item is already there, inbound will increment the quantity.--

    If not, then the item's information will be stored in a list of unregistered_items table and will be sent to 
    the frontend, where a user can have a form to fill in the data (name).

    If the quantity field is ever not defined the dafult value will be set to 1.
    

  */

  console.log("Request received for an inbound item");
  try {
    let { rfid, location, quantity } = req.body;

    if (quantity == null) {
      //failsafe for when the device doesn't send item quantity information.
      quantity = 1;
    }

    //Check to see if the item is in the registered items table.
    let result_item = await Item.findOne({ where: { rfid } });

    //Check to see if the item is in the unregistered items table.
    let unreg_result_item = await unreg_item.findOne({ where: { rfid } });

    //Update the quantities (decrement from old location if new(req) on is not same) and increment in to ).
    if (result_item !== null && unreg_result_item == null) {
      console.log("Item already registered");
      //If the location found  is different from the one in the requst body, decrement from the previous location.
      if (result_item.location !== location) {
        console.log("Item new location (in request) not same as one found");
        //Query the locations table to find the entry for the location where item was previously kept in.
        let location_to_decrease = await Location.findOne({
          where: { rfid: result_item.location },
        });
        location_to_decrease.no_of_items -= 1;
        await location_to_decrease.save();
      }

      let newLocationName;
      //update the number of items in the location where it was found.
      let location_to_increase = await Location.findOne({
        where: { rfid: location },
      });

      if (location_to_increase !== null) {
        location_to_increase.no_of_items += 1;
        await location_to_increase.save();

        //Update the item's location alias to be the one where it was inbound (also the one we are incerementing).
        newLocationName = location_to_increase.name;
      } else {
        newLocationName = "Unregistered Location";
      }

      //Update the item's location.
      result_item.location = location;
      result_item.locationalias = newLocationName;
      result_item.quantity += quantity;

      await result_item.save();

      res.status(201).json(result_item);
    } else if (unreg_result_item !== null && result_item == null) {
      //found in unregistered items and not in registered items table
      //increment the item's qty in unreg items table.
      unreg_result_item.quantity += quantity;

      //if req.location different from previous one, increment the no_of_items in new one and decrement in old one.
      if (location !== unreg_result_item.location) {
        //See if location was registered or not.
        let queriedLocation = await Location.fineOne({
          where: { rfid: location },
        });
        if (quriedLocation !== null) {
          queriedLocation.no_of_items += 1;
          await queriedLocation.save();
        } else {
          //put in a function here later to create an entry in the unreg locations table and add an item.
        }
      }

      await unreg_result_item.save();

      res.status(201).json(unreg_result_item);
    } else {
      //This else means the item was not found in the registered items and unregistered items table.

      //A new items could be scanned in a location that is already registered. Take the name and save in the unreg_item locationalias.
      let locationalias;
      let queried_location_of_item = await Location.findOne({
        where: { rfid: location },
      });

      if (queried_location_of_item !== null) {
        locationalias = queried_location_of_item.name;
      } else {
        locationalias = "Unregistered Location";
      }

      //Add the item to the table where unregistered items are placed.
      const new_item = await unreg_item.create({
        location,
        rfid,
        quantity,
        locationalias,
      });
      res.status(201).json({ new_item });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
    next(error);
  }
};

exports.itemOutbound = async (req, res, next) => {
  /*When an item is scanned as outbound at a particular location, 
    the database is first searched to see if the item has been previously registered (has been given a name).

    If not, then the item's information will be stored in a list of unregistered_items table and will be sent to 
    the frontend via a socket connection. Where a user can have a form to fill in the data (name, qty).

  */
  console.log("Request received for an outbound item");
  try {
    let { name, rfid, quantity } = req.body;

    let location = "Unlocated";
    //Find the item in teh registered items DB.
    let result_item = await Item.findOne({ where: { rfid } });
    let resultitemloc = result_item.location;

    //This attempts to reduce the quantity of itmes in a locaiton, but its not designed properly (only for discrete qty items).
    if (result_item !== null) {
      //update the number of items in the location where it was found.
      let location_to_reduce = await Location.findOne({
        where: { resultitemloc },
      });
      location_to_reduce.no_of_items -= 1;
      await location_to_reduce.save();

      //Update the item's location field.
      result_item.location = location;
      await result_item.save();
    } else {
      //In case the item is still not found, add the item to the table where unregistered items are placed.
      const new_item = await unreg_item.create({
        location,
        rfid,
      });
    }

    console.log(result_item);

    res.status(201).json(result_item);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
    next(error);
  }
};

//This is
exports.registerItem = async (req, res, next) => {
  console.log("Request received for registering an item");
  let { name, rfid, location, locationalias, quantity } = req.body;

  try {
    //Add the item to the items table where item details are complete.
    const item = await Item.create({
      name,
      location,
      rfid,
      locationalias,
      quantity,
    });

    //Remove the entry from the unregistered items table.
    console.log("item deleted");
    await unreg_item.destroy({ where: { rfid } });

    res.status(201).json(item);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
    next(error);
  }
};

exports.scanLocation = async (req, res, next) => {
  /*The scanner will always send a POST request when it scans a location,
  the server needs to know if the location is registered or not, 
  if it's registered it returns a simple "location found" message. 

  If it's not found it adds the new location to the table where unregistered locations are kept.
  */
  console.log("Request received for scanning a location");
  let { rfid } = req.body;

  let result_location = await Location.findOne({ where: { rfid } });
  try {
    if (result_location !== null) {
      //location found, do nothing.
      res.status(201).send("location found");
    } else {
      const location = await unreg_location.create({
        //Location not found create a new entry in the unreglocations table.
        rfid,
      });
      res.status(201).json({ location });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
    next(error);
  }
};

//This is a route meant for use with the front end.
exports.regLocation = async (req, res, next) => {
  console.log("Request received for registering a location");
  let { name, rfid } = req.body;

  let no_of_items = 0; //Initial Value.

  try {
    //Delete the location (being registered) from the unregistered locations table.
    let toDelete = await unreg_location.findOne({ where: { rfid } });
    console.log(toDelete);
    //In testing, sometimes a random rfid is sent which can't be found in the unreglocations table.
    if (toDelete != null) {
      await unreg_location.destroy({ where: { rfid } });
    }

    //Register the location and put it in the locations table.
    const location = await Location.create({
      name,
      no_of_items,
      rfid,
    });

    res.status(201).json(location);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
    next(error);
  }
};

exports.getItemById = async (req, res, next) => {
  console.log("Request received for fetching item by ID");
  try {
    let Id = req.params.id;
    item = await Item.findOne({ where: { Id } });

    res.status(200).json({ item });
  } catch (error) {
    console.log(error);
    res.status(500).send("Invalid Request");
    next(error);
  }
};

exports.getItemsByLocation = async (req, res, next) => {
  let { location } = req.body;

  try {
    let Allitems = Items.findAll({ where: { location } });
    res.status(200).json({ Allitems });
  } catch (error) {
    console.log(error);
    res.status(500).send("Invalid Request");
    next(error);
  }
};

exports.getAllUnregItems = async (req, res, next) => {
  try {
    let AllUnregItems = await unreg_item.findAll();
    res.status(200).json({ AllUnregItems });
  } catch (error) {
    console.log(error);
    res.status(500).send("Invalid Request");
    next(error);
  }
};

exports.getAllRegItems = async (req, res, next) => {
  try {
    let AllRegItems = await Item.findAll();
    res.status(200).json({ AllRegItems });
  } catch (error) {
    console.log(error);
    res.status(500).send("Invalid Request");
    next(error);
  }
};

exports.getAllRegLocations = async (req, res, next) => {
  try {
    let AllRegLocations = await Location.findAll();
    console.log("All registered locations: ", AllRegLocations);
    res.status(200).json({ AllRegLocations });
  } catch (error) {
    console.log(error);
    res.status(500).send("Invalid Request");
    next(error);
  }
};

exports.getAllUnregLocations = async (req, res, next) => {
  try {
    let AllUnregLocations = await unreg_location.findAll();
    console.log(AllUnregLocations);
    res.status(200).json({ AllUnregLocations });
  } catch (error) {
    console.log(error);
    res.status(500).send("Invalid Request");
    next(error);
  }
};

exports.getAllUnregItemsByLocation = async (req, res, next) => {
  let { location } = req.body;

  try {
    let Allitems = await unreg_item.findAll({ where: { location } });
    res.status(200).json({ Allitems });
  } catch (error) {
    console.log(error);
    res.status(500).send("Invalid Request");
    next(error);
  }
};

/*
--------------------------Backend API Routes---------------------------------


-------------------------Front End Functions---------------------------------
*/
