"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class unreg_location extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  unreg_location.init(
    {
      rfid: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "unreg_location",
      tableName: "unreg_locations",
    }
  );
  return unreg_location;
};
