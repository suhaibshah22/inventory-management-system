"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class unreg_item extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  unreg_item.init(
    {
      rfid: DataTypes.STRING,
      location: DataTypes.STRING,
      quantity: DataTypes.FLOAT,
      locationalias: DataTypes.STRING
    },
    {
      sequelize,
      modelName: "unreg_item",
      tableName: "unreg_items",
    }
  );
  return unreg_item;
};
