"use strict";

const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Item extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Item.init(
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      rfid: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      location: {
        //Location is going to be the RFID of the location.
        type: DataTypes.STRING,
        allowNull: false,
      },
      quantity:{
        type: DataTypes.FLOAT,
        allowNull: false
      },
      locationalias: {
        //Location Alias is going to be the Name of the location.
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      tableName: "items",
      modelName: "Item",
    }
  );
  return Item;
};
