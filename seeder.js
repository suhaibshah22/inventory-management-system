let axios = require("axios");

let unreg_items = [
  {
    rfid: "12345",
    location: "desk",
  },
  {
    rfid: "12345",
    location: "desk",
  },
  {
    rfid: "12345",
    location: "desk",
  },
  {
    rfid: "12345",
    location: "desk",
  },
  {
    rfid: "12345",
    location: "desk",
  },
  {
    rfid: "12345",
    location: "desk",
  },
  {
    rfid: "12345",
    location: "desk",
  },
  {
    rfid: "12345",
    location: "desk",
  },
  {
    rfid: "12345",
    location: "desk",
  },
];

let items = [
  {
    name: "cup1",
    rfid: "1",
    location: "desk",
  },
  {
    name: "cup2",
    rfid: "2",
    location: "desk",
  },
  {
    name: "cu3",
    rfid: "3",
    location: "desk",
  },
  {
    name: "cup4",
    rfid: "4",
    location: "desk",
  },
  {
    name: "cup5",
    rfid: "5",
    location: "desk",
  },
  {
    name: "cup6",
    rfid: "6",
    location: "desk",
  },
  {
    name: "cup7",
    rfid: "7",
    location: "desk",
  },
  {
    name: "cup8",
    rfid: "8",
    location: "desk",
  },
  {
    name: "9",
    rfid: "12345",
    location: "desk",
  },
];

let unreg_locations = [
  {
    rfid: "1",
  },
  {
    rfid: "2",
  },
  {
    rfid: "3",
  },
  {
    rfid: "4",
  },
  {
    rfid: "5",
  },
];

let locations = [
  {
    name: "home",
    rfid: "1",
  },
  {
    name: "yard",
    rfid: "2",
  },
  {
    name: "barn",
    rfid: "3",
  },
  {
    name: "farm",
    rfid: "4",
  },
  {
    name: "workshop",
    rfid: "5",
  },
];

// unreg_items.forEach(async (item) => {
//   const res = await axios.post("http://127.0.0.1:9000/inventory/inbound", item);
// });

unreg_locations.forEach(async (location) => {
  const res = await axios.post(
    "http://127.0.0.1:9000/inventory/location",
    location
  );
});
