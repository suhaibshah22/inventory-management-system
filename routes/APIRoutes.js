const express = require("express");
const controller = require("../controllers/APIControllers");
const cors = require("cors");
const router = express.Router();

//Cors Headers.

var corsOptions = {
  credentials: true,
  origin: "*",
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

router.all("/*", function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});

//Routes Designed for the Scanner Gateway MCU.
router.route("/inbound").post(controller.itemInbound);
router.route("/outbound").post(controller.itemOutbound);
router.route("/location").post(controller.scanLocation);

//--------------------Routes Designed for the frontend------------------------
router.route("/regitem").post(cors(corsOptions), controller.registerItem);

//Register a new location. Needs a JSON body with location name and rfid (might add )
router.route("/reglocation").post(cors(corsOptions), controller.regLocation);

//Get all unregistered items.
router.route("/unregitems").get(cors(corsOptions), controller.getAllUnregItems);

router.route("/getallitems").get(cors(corsOptions), controller.getAllRegItems);
//Get all items in a location.
router
  .route("/unregitemsbylocation")
  .get(cors(corsOptions), controller.getAllUnregItemsByLocation);
//router.route("/:id").get(controller.getItemById);
//router.route("/:location").get(controller.getItemsByLocation);

//Get all unregistered locations (they don't have a name).
router
  .route("/unreglocations")
  .get(cors(corsOptions), controller.getAllUnregLocations);

//Get all registered locations
router
  .route("/reglocations")
  .get(cors(corsOptions), controller.getAllRegLocations);

module.exports = router;
